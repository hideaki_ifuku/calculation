package xyz.ifuku.reversepolandcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.regex.Pattern;


public class MainActivity extends AppCompatActivity {

    Calculation Cal = new Calculation();

    private TextView text;




    //数値の処理をする
    private void updateSignNum(String s) {
        text.setText(Cal.stack(s));
    }

//式が既に代入されているときture
//Ansが既に使用されているときture
//イコール押した直後数値を入力すると次の計算を開始する

    //式等文字以外の処理をする
    private void updateSignString(String s) {

        if ("=".equals(s)) {                                   //イコールのときに処理
            equorButton(s);
        }else if ("C".equals(s)){                                    //クリアを押した時の処理
            clearButton();
        }
    }


    private void equorButton(String s){
        text.setText(Cal.result());
    }
    private void clearButton(){
        Cal = new Calculation();
        text.setText("0");
    }


//--------------------------------------------------------------------------------------------------

    //各種ボタンを押したときにそれぞれのメソッドに送る処理
    private void allocationButton(View b, final String s){
        ((Button)b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //数字は数字へそれ以外の文字は文字のメソッドへ
                if ("TEST".equals(s)){

                    text.setText(Cal.stack("1"));
                    text.setText(Cal.stack("+"));
                    text.setText(Cal.stack("2"));
                    text.setText(Cal.stack("*"));
                    text.setText(Cal.stack("3"));
                    text.setText(Cal.stack("+"));
                    text.setText(Cal.result());


                }
                if (Pattern.compile("[0-9]|[+*÷()]|[-]").matcher(s).find()) {
                    updateSignNum(s);
                }else{
                    updateSignString(s);
                }
            }
        });
    }

    @Override                                                       //ここにボタンの処理等→メイン画面に表示されているやつ
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //数値が表示されるテキストビューアーのリンク
        text = (TextView)findViewById(R.id.textView);

        //ボタンのリンクはallocationButtonで処理
        allocationButton(this.findViewById(R.id.button0), "0");
        allocationButton(this.findViewById(R.id.button1), "1");
        allocationButton(this.findViewById(R.id.button2), "2");
        allocationButton(this.findViewById(R.id.button3), "3");
        allocationButton(this.findViewById(R.id.button4), "4");
        allocationButton(this.findViewById(R.id.button5), "5");
        allocationButton(this.findViewById(R.id.button6), "6");
        allocationButton(this.findViewById(R.id.button7), "7");
        allocationButton(this.findViewById(R.id.button8), "8");
        allocationButton(this.findViewById(R.id.button9), "9");
        allocationButton(this.findViewById(R.id.button_puls), "+");
        allocationButton(this.findViewById(R.id.button_minus), "-");
        allocationButton(this.findViewById(R.id.button_mas), "*");
        allocationButton(this.findViewById(R.id.button_wal), "÷");
        allocationButton(this.findViewById(R.id.button_equor), "=");
        allocationButton(this.findViewById(R.id.button_C), "C");
        allocationButton(this.findViewById(R.id.button_test), "TEST");




        //ListViewのセット
        ListView listView = new ListView(this);
        setContentView(listView);

        //データの追加
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        adapter.add("a");
        adapter.add("b");
        adapter.add("c");

        listView.setAdapter(adapter);




    }



    //↓今のところ用は無い↓
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
