package xyz.ifuku.reversepolandcalculator;


import android.util.Log;

import java.util.regex.Pattern;

public class Check_Number {
    public boolean Chk_Num(String s){
        if(Pattern.compile("[0-9]|[+*÷()]|[-]").matcher(s).find()) {    //四則演算に使う文字以外はエラーを返す
            return true;
        }else{
            Log.d("エラー", "Check_NumberクラスChk_Numメソッドにて入力された文字が四則演算以外の文字が入力が確認検出されました。");
            return false;
        }
    }
}
