package xyz.ifuku.reversepolandcalculator;

import android.util.Log;


import java.util.Stack;
import java.util.regex.Pattern;


public class Calculation {



    public final static String ERROR_DEVIDED_BY_ZERO = "ERROR_DEVIDED_BY_ZERO";
    public final static String PLAS ="+";
    public final static String MINUS = "-";
    public final static String MULTIPLY = "*";
    public final static String DIVISION = "/";

    public String reverse_res = "";                                            //リバースの返す値。
    private String poland_res = "";                                             //ポーランドに変換した時の値
    private String nowNumber = "";
    private boolean second_Cal = false;


    //空白で区切られた数値、式を計算する
    private String results(String num) {
        Stack<Double> a_Stack = new Stack<Double>();    //計算スタック 作成　<インテガー>は型のラッパー型を指定する
        double a = 0;                                    //スタックの計算処理用変数
        double b = 0;                                    //上同
        boolean error = false;                                    //0で割られた場合Turu

        String[] Number = num.split(" ");//空白ごとに区切って配列に代入

        for (int i = 0; i < Number.length; i++) {


            if(PLAS.equals(Number[i])){
                a = a_Stack.pop();
                b = a_Stack.pop();
                a_Stack.add(b + a);
                continue;
            }
            if(MINUS.equals(Number[i])){
                a = a_Stack.pop();
                b = a_Stack.pop();
                a_Stack.add(b - a);
                continue;
            }
            if(MULTIPLY.equals(Number[i])){
                a = a_Stack.pop();
                b = a_Stack.pop();
                a_Stack.add(b * a);
                continue;
            }
            if(DIVISION.equals(Number[i])){
                a = a_Stack.pop();
                b = a_Stack.pop();
                if(a == 0){                             //0で割られた場合エラーにする
                    error = true;
                    break;
                }
                a_Stack.add(b / a);
                continue;
            }
            //演算子以外はスタックに格納する
            a_Stack.push(Double.valueOf(Number[i]));
        }
        if (error){
            return ERROR_DEVIDED_BY_ZERO;
        }else {
            return String.valueOf(a_Stack.pop());
        }

    }

    //----------------------------------------------------------------------------------------------

    //イコールを押した処理
    public String result(){
        //計算できない文字があったり計算式が入力されていない場合はエラー文を返して終了。
        if (!Chk_Num(nowNumber)){
            Log.d("エラー(86)","Calculation.java resultメソッドにてエラー。計算式がに入力されていません。");
            return "0";
        }

        Reverse_Poland(nowNumber);

        String s = results(poland_res);
        //エラー文の場合はnowNomberに代入しない
        if(!ERROR_DEVIDED_BY_ZERO.equals(s))nowNumber = s;

        //答えが整数型の場合は整数型で表表示する
        //スプリットで後ろの.0を取って前の数字を出力する
        if(Pattern.compile("0").matcher(nowNumber.substring(nowNumber.length()-1)).find()){
            String[] ss = nowNumber.split("\\.0");
            s = ss[0];
            nowNumber = s;
            second_Cal = true;
            return nowNumber;
        }

        second_Cal = true;
        return s;
    }



    public String stack(String s){

        //イコール押した後数値を押すと新しく計算を開始する
        if (Pattern.compile("[0-9]").matcher(s).find() && second_Cal){
            clear();
            nowNumber = "";
            second_Cal = false;
        }
        second_Cal = false;
        //何も文字が入力されていない状態だとリターンで終了
        if("".equals(nowNumber) && Pattern.compile("[+*÷]|[-]|[0]").matcher(s).find())return "0";

        //式の場合に二重チェック
        if(Pattern.compile("[+*÷]|[-]").matcher(s).find()){
            //一番最後の文字と現在の文字を比較
            if(Pattern.compile("[+*÷]|[-]").matcher(nowNumber.substring(nowNumber.length()-1)).find()){
                return nowNumber;
            }

        }


        if(Chk_Num(s)){
            nowNumber += s;
            return nowNumber;
        }else{
            return "不正な式です。";
        }
    }


/*
    //(10+(20+30)+40)
    private String Parenthesis(String s){
        String parenthesis_Start_Temp = "";
        String[] num = s.split("");             //空白ごとに区切って配列に代入

        for (int i = 0; i < num.length; i++){
            //括弧以外の処理の場合
            if(!Pattern.compile("[(]|[)]").matcher(num[i]).find()){
                returnStack += num[i];
                continue;
            }
            if(Pattern.compile("[(]").matcher(num[i]).find())){
                //その時点から以降の数値を全て代入
                for (int k = i; k < num.length; k++){
                    parenthesis_Start_Temp += num[k];
                }
                return Parenthesis(parenthesis_Start_Temp);
                //parenthesisCnt++;

            }
            if(Pattern.compile("[)]").matcher(num[i]).find())){

            }

        }

    }
    */


    //文字のエラーチェック
    public boolean Chk_Num(String s){
        //文字が何もない場合falseを返す
        if("".equals(s))return false;

        String[] chk_Num_Temp = s.split("");//空白ごとに区切って配列に代入

        for (int i = 1; i < chk_Num_Temp.length; i++) {

            if (Pattern.compile("[0-9]|[+*÷()]|[-]").matcher(chk_Num_Temp[i]).find()) {    //四則演算に使う文字以外はエラーを返す

            } else {
                Log.d("エラー", "Check_NumberクラスChk_Numメソッドにて入力された文字が四則演算以外の文字が入力が確認検出されました。");
                return false;
            }
        }
        return true;
    }



/*
    public int fact(int n){
        if(n < 1)return 1;
        else fact(n -1);
    }
*/



    //空白がない状態で渡すと計算できる逆ポーランドをReverse_resに渡す
    private String Reverse_Poland(String s) {
        String source;
        Stack<String> a_Stack = new Stack<String>();         //出力する値のキュー


        clear();
        Reverses(s);                                 //数字と式を区別する
        source = reverseResult();                  //結果をソースに代入
        String[] Nums = source.split(" ");             //空白ごとに区切って配列に代入

        for (int i = 0; i < Nums.length; i++) {         //全ての振り分ける




            //最後の処理の場合スタックの中身を全部詰め込んで終了
            if (Nums.length - 1 == i) {
                poland_res += Nums[i];
                //スタックの中がきれるまで繰り返す
                while (!a_Stack.empty()) {
                    poland_res += " " + a_Stack.pop();
                }
                break;
            }




            //プラスかマイナスの場合Ture
            if (Pattern.compile("[+]|[-]").matcher(Nums[i + 1]).find()) {

                //スタックの中身が+か-かNullの場合と*と/がある場合を分ける
                if (a_Stack.empty() || Pattern.compile("[+]|[-]").matcher(a_Stack.peek()).find()) {
                    poland_res += Nums[i];
                    a_Stack.add(Nums[i + 1]);
                    i++;
                } else if (Pattern.compile("[*]|[/]").matcher(a_Stack.peek()).find()) {
                    poland_res += Nums[i];
                    while (!a_Stack.empty()) {
                        poland_res += " " + a_Stack.pop();
                    }
                    a_Stack.add(Nums[i + 1]);
                    i++;
                } else {
                    Log.d("エラー", "CalculationクラスNextPolandメソッドで不明なエラー");
                }
                //iの数値を代入してa_queに格納する
            } else if (Pattern.compile("[*]|[/]").matcher(Nums[i + 1]).find()) {
                poland_res += Nums[i];
                a_Stack.add(Nums[i + 1]);
                i++;
            }
            poland_res += " ";
        }
        return poland_res;
    }




    //文字列ので処理する場合のメソッド
    public void Reverses(String num){
        String[] Number = num.split("");    // 一文字ごとに区切って配列に代入

        for (int i = 1; i < Number.length; i++) {
            Reverse(Number[i]);
        }
    }

    //入力された文字を数値と式を空白で区切るにするメソッド
    public void Reverse(String num) {

        if (Pattern.compile("[0-9]").matcher(num).find()) { //引数が数値だった場合ture
            reverse_res += num;
        } else {    //戻り値に1-9が含まれている場合ture
            if (Pattern.compile("[0-9]").matcher(reverse_res).find()){          //一番最初に数値以外の何を入力しても無視する
                if("÷".equals(num)){
                    reverse_res += " " + "/" + " ";                                 //÷の時こっちの内部では/とする
                }else {
                    reverse_res += " " + num + " ";                                 //プラスマイナスも一番最初は無視する
                }
            }else{                                                              //小数点配慮しない
                Log.d("エラー","CalculationクラスのReverseメソッドで一番最初に数値以外が入力されました。");
            }
        }
    }









    //Reverseした時の戻り値で返す時
    public String reverseResult(){
        return reverse_res;
    }

    public void clear(){
//        nowNumber = "";
        reverse_res = "";
        poland_res = "";
    }

}

